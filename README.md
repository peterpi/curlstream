# curlstream: iostreams using libcurl #

Version 0.0.1.  **Work-in-progress, do not use**.

## Rationale ##

In C#, Java, Python and many other languages, reading from a network resouce is as easy as reading from a file.
In C++ it is unnecessarily complex.

## Introduction ##

`curlstream` is a class that gives a `std::iostream` interface to a libcurl `CURL` structure.
It allows you to do the following:

	curlstream c ("http://www.example.com");
	cout << c.rdbuf();

## Design Goals ##



### Simplicity for the common case ###

Doing a straightforward read of a network resource should be no more complex than using an `ifstream` to read a file.

### Unobtrusive ###

Power users should be allowed to manipulate the curl handle however they please.
I have no desire to stop you from using libcurl's existing interface.
The underlying `CURL` structure is a public field of the `stream` (actually the `streambuf`)


## Tutorial ##

### HTTP GET ###

To perform an HTTP(S) GET, just create a `curlstream` and use it:

	curlstream in ("http://www.example.com");
	string s;
	in >> s;
	// etc.

### HTTP POST ###

To perform an HTTP(S) POST, follow these steps:

* Create a curlstream by calling `curlstream::post (const char * url)` .
* Write your POST data to the `curlstream` as you would any other `std::ostream`.
* Call `curlstream::close_out()` when you have posted all your data.
* Read the response.

e.g.

	curlstream cs = curlstream::post ("http://httpbin.org/post");
	cs << "curlstreams=wonderful";
	cs.close_out();
	
	string line;
	while (cs) {
		getline (cs, line);
		if (cs.eof())
			break;
		cout << line;
	}

Power users note:  This sets `CURLOPT_POST` and sets a request header `Transfer-Encoding: chunked` .

### Custom Request Headers ###

To add custom headers to the request, access the `CURL` handle and call `curl_easy_setopt` with `CURLOPT_HTTPHEADER`.
For your convenience, `curlstreambuf` has a public field `curl_slist headers` which is cleaned up in the `curlstreambuf` destructor.  This means you can do something like:

	curlstream cs ("http://www.example.com");
	curl_slist * & headers = cs.buf.headers;
	headers = curl_slist_append (headers, "SomeHeader: SomeValue");
	headers = curl_slist_append (headers, "OtherHeader: OtherValue");
	curl_easy_setopt (cs.buf.curl, CURLOPT_HTTPHEADER, headers);

... and `headers` will be freed automatically when `cs` goes out of scope.

### Other Custom Behaviour ###

After creating the `curlstream`, access the `CURL` instance and set whatever options you require.

`curl_multi_perform` is not called until the first call to either `streambuf::underflow`, `streambuf::overflow` or `streambuf::sync`.  That is; it is not called until you perform a `<<` or `>>` operation on the `curlstream`.


## License ##

Copyright (c) 2016 Peter Pimley

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
