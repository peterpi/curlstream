

LDLIBS+=-lcurl

srcs=$(wildcard *.cpp)
objs+=$(subst .cpp,.o, $(srcs))

CXXFLAGS+=--std=c++11 -Wall -Werror -Wno-unused-function

.PHONY: all
all: test docs

test: main.o
	$(CXX) -o $@ $(objs) $(LDFLAGS) $(LDLIBS)

.PHONY: run
run: test
	./test http://httpbin.org/post

.PHONY: clean
clean:
	$(RM) $(objs)

.PHONY: docs
docs: README.html

%.html: %.md
	markdown $^ > $@

# Dependencies
CXXFLAGS+=-MD
deps=$(subst .o,.d, $(objs))
.PHONY: clean_deps
clean: clean_deps
clean_deps:
	$(RM) $(deps)
-include $(deps)
