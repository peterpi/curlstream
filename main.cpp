
#include "curlstream.h"
#include <string>

using namespace std;

static void show_stream (istream& in)
{
	cout << in.rdbuf();
	return;
	string s;
	while (in)
	{
		getline (in, s);
		if (in.eof())
			break;
		cout << ">>>"<<s<<"<<<"<<endl;
	}
}
		
		

static void get ()
{
	curlstream in ("http://www.example.org");
	show_stream (in);
}


static void post ()
{
	curlstream in = curlstream::post ("http://httpbin.org/post");
	in << "hello=world";
	in.close_out();
	show_stream (in);
}

int main (int argc, char ** argv)
{
	get();
	post();
}

