/*
Copyright (c) 2016 Peter Pimley

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

*/


#include <iostream>
#include <stdexcept>
#include <sys/select.h>
#include <curl/curl.h>
#include <string.h>
#include <vector>
#include <assert.h>



class curlstreambuf : public std::streambuf
{

public:

	CURLM * multi;
	CURL * curl;
	curl_slist * headers;

	curlstreambuf (const char * url) :
		multi (init_multi_or_throw()),
		curl (init_curl_or_throw()),
		headers (0),
		out_closed(false),
		numCBCalls(0),
		get (CURL_MAX_WRITE_SIZE),
		put(1024),
		numBytesPosted(0)
	{

		setg (&get[0], &get[get.size()], &get[get.size()]);
		setp (&put[0], &put[put.size()]);

		curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, s_write_cb);
		curl_easy_setopt (curl, CURLOPT_WRITEDATA, this);
		curl_easy_setopt (curl, CURLOPT_READFUNCTION, s_read_cb);
		curl_easy_setopt (curl, CURLOPT_READDATA, this);

		curl_easy_setopt (curl, CURLOPT_URL, url);

		curl_easy_pause (curl, CURLPAUSE_ALL);
		CURLMcode code = curl_multi_add_handle (multi, curl);
		if (code != CURLM_OK)
			throw init_err();
	}

	curlstreambuf (curlstreambuf && cb) :
		multi (cb.multi),
		curl (cb.curl),
		headers (cb.headers),
		out_closed (cb.out_closed),
		numCBCalls (cb.numCBCalls),
		get (std::move (cb.get)),
		put (std::move (cb.put)),
		numBytesPosted (cb.numBytesPosted)
	{
		cb.curl = 0;
		cb.multi = 0;
		cb.headers = 0;
	}

	~curlstreambuf ()
	{
		bool moved = multi == 0;
		if (moved)
			return;
		curl_slist_free_all (headers);
		curl_multi_remove_handle (multi, curl);
		curl_easy_cleanup (curl);
		curl_multi_cleanup (multi);
	}

	class init_err : public std::runtime_error
	{
	public:
		init_err () : std::runtime_error("hello") {}
	};

	void close_out ()
	{
		out_closed = true;
		pubsync();
	}

protected:

	int_type underflow ()
	{
		// std::cerr<<"Underflow"<<std::endl;
		curl_easy_pause (curl, CURLPAUSE_CONT);
		while (true)
		{
			bool running = pump();

			// If the callback ran, it would have reset the get pointers.
			bool gotSome = gptr() < egptr();
			if (gotSome)
				return gptr()[0];

			// If pump returns false, it means end-of-file.
			if (!running)
				return -1;
		}
	}

	int_type overflow (char_type c)
	{
		// std::cerr<<"overflow"<<std::endl;
		bool synced = sync();
		if (synced)
		{
			// store "c".
			*pptr() = c;
			setp (pptr()+1, epptr());
			return c;
		}
		else
		{
			return -1;
		}
	}


	int sync ()
	{
		std::cerr<<"sync"<<std::endl;
		curl_easy_pause (curl, CURLPAUSE_CONT);
		size_t bytesToPost = pptr() - pbase();
		numBytesPosted = 0;
		while (true)
		{
			bool running = pump();
			if (numBytesPosted == bytesToPost)
			{
				std::cerr<<"sync OK; wrote everything"<<std::endl;
				setp (&put[0], &put[put.size()]);
				numBytesPosted = 0;
				return 0;
			}
			if (!running)
			{
				std::cerr<<"sync failed, transfer no longer in progress"<<std::endl;
				return -1;
			}
		}
	}
		

			


private:
	bool out_closed;
	unsigned numCBCalls;

	std::vector<char> get, put;
	size_t numBytesPosted;

	static CURLM * init_multi_or_throw ()
	{
		CURLM * m = curl_multi_init();
		if (!m)
			throw init_err();
		return m;
	}

	static CURL * init_curl_or_throw ()
	{	
		CURL * c = curl_easy_init ();
		if (!c)
			throw init_err();
		return c;
	}

	bool pump ()
	{
		// std::cerr<<"pump"<<std::endl;
		fd_set read_set;
		FD_ZERO (&read_set);
		fd_set write_set;
		FD_ZERO (&write_set);
		fd_set ex_set;
		FD_ZERO (&ex_set);

		int max_fd = 0;
		CURLMcode fdset_ret= curl_multi_fdset (multi, &read_set, &write_set, &ex_set, &max_fd);
		// std::cerr<<"max_fd="<<max_fd<<std::endl;
		if (fdset_ret != CURLM_OK)
		{
			std::cerr<<"multi_fdset failed with "<<fdset_ret<<std::endl;
			assert(false);
		}

		// Curl can tell us how long it would like to block for.
		long millis = 0;
		if (max_fd >= 0)
			curl_multi_timeout (multi, &millis);
		else
			millis = 100; // See "man curl_multi_fd_set".
		// std::cerr<<"Wait for "<<millis<<std::endl;

		timeval timeout;
		timeout.tv_sec = millis / 1000;
		timeout.tv_usec = (millis % 1000) * 1000;
		int numready = select (max_fd+1, &read_set, &write_set, &ex_set, &timeout);
		(void)numready; // std::cerr<<numready<<" filehandles ready"<<std::endl;
		int running_handles = 0;
		CURLMcode perform = curl_multi_perform (multi, &running_handles);
		if (perform != CURLM_OK)
		{
			std::ostringstream o;
			o << "curl_multi_perform failed with " << perform;
			throw std::runtime_error(o.str());
		}
		bool still_running = running_handles > 0;
		// std::cerr<<"pump has "<<running_handles<<" running_handles"<<std::endl;
		return still_running;
	}
		

	static size_t s_write_cb (char * buf, size_t size, size_t nitems, void * ptr)
	{
		curlstreambuf * const me = (curlstreambuf*) ptr;
		size_t avail = size * nitems;
		return me->write_cb (buf, avail);
	}

	size_t write_cb (char * buf, size_t avail)
	{
		++numCBCalls;
		bool haveData = gptr() < egptr();
		if (haveData) {
			std::cerr<<"write_cb when already have data."<<std::endl;
			return CURL_WRITEFUNC_PAUSE;
		}
		size_t capacity = get.size();
		size_t copyBytes = std::min (avail, capacity);
		// std::cerr<<"Provided with "<<avail<<", have space for "<<capacity<<", writing "<<copyBytes<<std::endl;
		// std::string s (buf, avail);
		// std::cerr<<"Provided with \""<<s<<"\""<<std::endl;
		char_type * dest = eback();
		memcpy (dest, buf, copyBytes);
		setg (dest, dest, &dest[copyBytes]);
		return copyBytes;
	}

	static size_t s_read_cb (char * buf, size_t size, size_t nitems, void * ud)
	{
		curlstreambuf * const me = (curlstreambuf*) ud;
		size_t avail = size * nitems;
		return me->read_cb(buf, avail);
	}

	size_t read_cb (char * buf, size_t space)
	{
		size_t avail = (pptr()-pbase()) - numBytesPosted;
		if (!avail) {
			if (out_closed)
			{
				std::cerr<<"read_cb when closed, finishing."<<std::endl;
				return 0;
			}
			else
			{
				std::cerr<<"read_cb when starved"<<std::endl;
				return CURL_READFUNC_PAUSE;
			}
		}
		size_t copyAmount = std::min (avail, space);
		std::cerr<<"read_cb allows "<<space<<", we have "<<avail<<", ("<<(pptr()-pbase())<<" with "<<numBytesPosted<<" copied), copying "<<copyAmount<<std::endl;
		char * src = & put[numBytesPosted];
		memcpy (buf, src, copyAmount);
		numBytesPosted += copyAmount;
		return copyAmount;
	}
};


class curlstream : public std::iostream
{
public:
	curlstream (const char * url) :
		std::iostream (&buf),
		buf(url)
	{}

	curlstream (curlstream && cs) :
		buf (std::move(cs.buf))
	{}


	void close_out ()
	{
		buf.close_out();
	}

	static curlstream post (const char * url)
	{
		curlstream s (url);
		curlstreambuf & sb = s.buf;
		CURL * curl = sb.curl;
		curl_easy_setopt (curl, CURLOPT_POST, 1);
		sb.headers=curl_slist_append (sb.headers, "Transfer-Encoding: chunked");
		curl_easy_setopt (curl, CURLOPT_HTTPHEADER, sb.headers);
		return s;
	}

	static curlstream post_data (const char * url, const char * data)
	{
		curlstream s (url);
		CURL * curl = s.buf.curl;
		curl_easy_setopt (curl, CURLOPT_POST, 1);
		curl_easy_setopt (curl, CURLOPT_COPYPOSTFIELDS, data);
		return s;
	}


	curlstreambuf buf;
};

